@extends('template.main')


@section('content')
    <div ui-view class="app-body" id="view">
        <!-- ############ PAGE START-->
        <div class="padding">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1>DATA [LAPORAN REALISASI KEGIATAN]</h1>
                            <form action="{{ url('get-selected') }}" method="POST">

                                <div class="row mt-5">
                                    @csrf
                                    @foreach ($data as $item)
                                        <input type="hidden" name="pak_id" value="">
                                    @endforeach
                                    <div class="col-md col-sm col-lg">
                                        <label for="bulan">BULAN:</label>
                                        <select class="custom-select form-control" id="bulan" name="bulan">
                                            <option value="">Pilih Bulan Realisasi</option>
                                            <option value="januari">Januari</option>
                                            <option value="februari">Februari</option>
                                            <option value="maret">Maret</option>
                                            <option value="april">April</option>
                                            <option value="mei">Mei</option>
                                            <option value="juni">Juni</option>
                                            <option value="juli">Juli</option>
                                            <option value="agustus">Agustus</option>
                                            <option value="september">September</option>
                                            <option value="oktober">Oktober</option>
                                            <option value="november">November</option>
                                            <option value="desember">Desember</option>
                                        </select>
                                    </div>
                                    <div class="col-md col-sm col-lg">
                                        <label for="dana">SUMBER DANA:</label>
                                        <select class="custom-select form-control" id="dana" name="dana">
                                            <option value="">Pilih Sumber Dana</option>
                                            <option value="APBD Kabupaten Pamekasan">APBD Kabupaten Pamekasan</option>
                                            <option value="APBD Provinsi">APBD Provinsi</option>
                                            <option value="APBN">APBN</option>
                                        </select>
                                    </div>
                                    <div class="col-md col-sm col-lg">
                                        <button type="submit" class="md-btn md-raised m-b-sm blue" role="button"
                                            style="margin-top: 35px">
                                            <i class="bi bi-check-lg"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                </div>
                            </form>
                        </div>
                        <div class="box-body">
                            <form action="{{ route('report.up') }}" method="POST">
                                @csrf
                                <div class="table-responsive">
                                    <table class="table table-bordered justify-content-centery align-items-center"
                                        id="table">
                                        <thead>
                                            <tr>
                                                <th class="tg-0pky" rowspan="2">No</th>
                                                <th class="tg-0pky" rowspan="2">SUB KEGIATAN</th>
                                                <th class="tg-0pky" rowspan="2">ANGGARAN</th>
                                                <th class="tg-0pky" colspan="2">REALISASI KEGIATAN(%)</th>
                                                <th class="tg-0pky" colspan="2">REALISASI KEUANGAN(Rp.)</th>
                                                <th class="tg-0pky" rowspan="2">KENDALA</th>
                                            </tr>
                                            <tr>
                                                <th class="tg-0pky">BULAN LALU</th>
                                                <th class="tg-0pky">BULAN INI</th>
                                                <th class="tg-0pky">BULAN LALU</th>
                                                <th class="tg-0pky">BULAN INI</th>
                                            </tr>
                                        </thead>
                                        <tbody id="body_report">
                                            @if ($selected == null)
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            @else
                                                @foreach ($selected as $item)
                                                    <tr class="data-r">
                                                        <input type="hidden" class="form-control" name="bulan"
                                                            value="{{ $item->bulan }}">
                                                        <input type="hidden" class="form-control" name="dana"
                                                            value="{{ $item->dana }}">
                                                        <input type="hidden" class="form-control"
                                                            name="id-{{ $item->id }}" value="{{ $item->id }}">
                                                        <td id="no">{{ $loop->iteration }}</td>
                                                        <td id="kegiatan">{{ $item->kegiatan }}</td>
                                                        <td id="anggaran">{{ $item->anggaran }}</td>
                                                        <td id="klalu">
                                                            {{ $item->kegiatan_lalu != null ? $item->kegiatan_lalu : '' }}
                                                        </td>
                                                        <td id="inkegiatan">
                                                            <input type="number" class="form-control"
                                                                name="kegiatan-{{ $item->id }}" id="kegiatan"
                                                                value="{{ $item->kegiatan_sekarang != null ? $item->kegiatan_sekarang : '' }}"
                                                                readonly>
                                                        </td>
                                                        <td id="molalu">
                                                            {{ $item->keuangan_lalu != null ? $item->keuangan_lalu : '' }}
                                                        </td>
                                                        <td id="keuangan">
                                                            <input type="number" class="form-control"
                                                                name="keuangan-{{ $item->id }}" id="keuangan"
                                                                value="{{ $item->keuangan_sekarang != null ? $item->keuangan_sekarang : '' }}">
                                                        </td>
                                                        <td id="kendala">
                                                            <input type="text" class="form-control"
                                                                name="kendala-{{ $item->id }}" id="kendala"
                                                                value="{{ $item->kendala != null ? $item->kendala : '' }}">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                    <div class="col">
                                        <button type="submit" class="md-btn md-raised m-b-sm w-xs blue"
                                            role="button">SIMPAN</button>
                                        <button type="button" class="md-btn md-raised m-b-sm w-xs orange text-white"
                                            id="reff" role="button"
                                            onclick="window.location.href = '{{ route('report.index') }}'">RELOAD</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ############ PAGE END-->
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $("#table").DataTable();
        });
    </script>
    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }

    </style>
@endpush
