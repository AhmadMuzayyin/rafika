@extends('template.main')


@section('content')
    <div ui-view class="app-body" id="view">
        <!-- ############ PAGE START-->
        {{-- @dd(session()->get('pak_id')) --}}
        <div class="padding">
            @if ($page == false)
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <h1>DATA [SUB KEGIATAN]</h1>
                            </div>
                            <div class="box-body">
                                @if (Auth()->user()->isAdmin == false)
                                    <a href="{{ route('activity.create') }}"
                                        class="md-btn md-raised m-b-sm w-xs blue text-decoration-none text-white"
                                        role="button">TAMBAH</a>
                                @endif
                                <div class="table-responsive">
                                    <table class="table" id="table">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>NOMOR REKENING</th>
                                                <th>SUB KEGIATAN</th>
                                                <th>ANGGARAN</th>
                                                <th>DANA</th>
                                                <th>KETERANGAN</th>
                                                @if (Auth()->user()->isAdmin == false)
                                                    <th>Aksi</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data as $k)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $k->rek }}</td>
                                                    <td>{{ $k->kegiatan }}</td>
                                                    <td id="tdanggara">@formatUang($k->anggaran)</td>
                                                    <td>{{ $k->dana }}</td>
                                                    <td>{{ $k->keterangan }}</td>
                                                    @if (Auth()->user()->isAdmin == false)
                                                        <td>
                                                            <div class="btn-group">
                                                                <form action="{{ route('activity.edit', $k->id) }}"
                                                                    method="GET" class=" d-inline-block">
                                                                    <button type="submit"
                                                                        class="md-btn md-raised m-b-sm blue" role="button"
                                                                        style="border: 0px">
                                                                        <i class="bi bi-pencil-square"></i>
                                                                    </button>
                                                                </form>
                                                                <form action="{{ route('activity.destroy', $k->id) }}"
                                                                    class=" d-inline-block mx-2">
                                                                    @csrf
                                                                    <button type="button"
                                                                        class="md-btn md-raised m-b-sm red deleteActivity"
                                                                        role="button" style="border: 0px">
                                                                        <i class="bi bi-trash-fill"></i>
                                                                    </button>
                                                                </form>
                                                            </div>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @elseif($page == 'edit')
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <a href="{{ route('activity.index') }}"
                                    class="md-btn md-raised m-b-sm w-xs blue text-decoration-none text-white"
                                    role="button">View</a>
                            </div>
                            <div class="box-body">
                                <h5>Form Entry</h5>
                                <form id="updateActivity" action="{{ route('kegiatan.up') }}">
                                    @csrf
                                    {{-- @dd($data->laporan->toArray()) --}}
                                    <input type="hidden" name="id" value="{{ $data->id }}">
                                    <div class="row">
                                        <div class="col-md -col-sm col-lg">
                                            <label for="rek">NOMOR REKENING</label>
                                            <small class="text-danger">Mohon diisi dengan lengkap dan benar</small>
                                            <input type="text" class="form-control" id="rek" name="rek"
                                                placeholder="Contoh: 4.01.03.20.2.05.01" required autofocus
                                                value="{{ $data->rek }}">
                                        </div>
                                        <div class="col-md -col-sm col-lg">
                                            <label for="dana">SUMBER DANA</label>
                                            <select class="form-control" id="dana" name="dana" required>
                                                <option value="">---PILIH SUMBER DANA---</option>
                                                <option value="APBD Kabupaten Pamekasan"
                                                    {{ $data->dana == 'APBD Kabupaten Pamekasan' ? 'selected' : '' }}>
                                                    APBD
                                                    Kabupaten Pamekasan</option>
                                                <option value="APBD Provinsi"
                                                    {{ $data->dana == 'APBD Provinsi' ? 'selected' : '' }}>
                                                    APBD Provinsi</option>
                                                <option value="APBN" {{ $data->dana == 'APBN' ? 'selected' : '' }}>
                                                    APBN</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md col-sm col-lg">
                                            <label for="subkegiatan">NAMA SUB KEGIATAN</label>
                                            <input type="text" class="form-control" name="subkegiatan" required
                                                value="{{ $data->nama }}">
                                        </div>
                                        <div class="col-md col-sm col-lg">
                                            <label for="pengadaan">JENIS PENGADAAN</label>
                                            <select class="form-control" name="pengadaan" id="pengadaan"
                                                onchange="select()" required>
                                                <option value="">---PILIH JENIS PENGADAAN---</option>
                                                <option value="Konstruksi"
                                                    {{ $data->pengadaan == 'Konstruksi' ? 'selected' : '' }}>Konstruksi
                                                </option>
                                                <option value="Barang"
                                                    {{ $data->pengadaan == 'Barang' ? 'selected' : '' }}>Barang</option>
                                                <option value="Konsultasi"
                                                    {{ $data->pengadaan == 'Konsultasi' ? 'selected' : '' }}>Konsultasi
                                                </option>
                                                <option value="Jasa Lainnya"
                                                    {{ $data->pengadaan == 'Jasa Lainnya' ? 'selected' : '' }}>Jasa
                                                    Lainnya</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md col-sm col-lg">
                                            <label for="anggaran">ANGGARAN</label>
                                            <small class="text-info">Ditulis tanpa titik dan koma</small>
                                            <input type="text" class="form-control" name="anggaran" required
                                                value="{{ $data->anggaran }}">
                                        </div>
                                        <div class="col-md col-sm col-lg">
                                            <label for="pelaksanaan">METODE PELAKSANAAN</label>
                                            <select class="form-control" name="pelaksanaan" id="pelaksanaan" required>
                                                <option value="">---PILIH METODE PELAKSANAAN---</option>
                                                <option value="Tender"
                                                    {{ $data->pelaksanaan == 'Tender' ? 'selected' : '' }}>Tender
                                                </option>
                                                <option value="Penunjukan Langsung"
                                                    {{ $data->pelaksanaan == 'Penunjukan Langsung' ? 'selected' : '' }}>
                                                    Penunjukan Langsung</option>
                                                <option value="Pengadaan Langsung"
                                                    {{ $data->pelaksanaan == 'Pengadaan Langsung' ? 'selected' : '' }}>
                                                    Pengadaan Langsung</option>
                                                <option value="ePurchasing"
                                                    {{ $data->pelaksanaan == 'ePurchasing' ? 'selected' : '' }}>
                                                    ePurchasing
                                                </option>
                                                <option value="Swakelola"
                                                    {{ $data->pelaksanaan == 'Swakelola' ? 'selected' : '' }}>Swakelola
                                                </option>
                                                <option value="Seleksi"
                                                    {{ $data->pelaksanaan == 'Seleksi' ? 'selected' : '' }}>Seleksi
                                                </option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col">
                                            <label for="kegiatan">BENTUK KEGIATAN</label>
                                            <input type="text" class="form-control" name="kegiatan" required
                                                value="{{ $data->kegiatan }}">
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="laporan">LAPORAN</label>
                                                    <div class="lpError"></div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="laporan1"
                                                                name="laporan[]" value="DAK"
                                                                {{ $data->laporan == '["DAU"]' ? 'checked' : ($data->laporan == '["DAU","DAK","DBHC"]' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="laporan1">
                                                                DAU
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="laporan1"
                                                                name="laporan[]" value="DAK"
                                                                {{ $data->laporan == '["DAK"]' ? 'checked' : ($data->laporan == '["DAU","DAK","DBHC"]' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="laporan1">
                                                                DAK
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="laporan2"
                                                                name="laporan[]" value="DBHC"
                                                                {{ $data->laporan == '["DBHC"]' ? 'checked' : ($data->laporan == '["DAU","DAK","DBHC"]' ? 'checked' : '') }}>
                                                            <label class="form-check-label" for="laporan2">
                                                                DBHC
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="program">PROGRAM BUPATI</label>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="program"
                                                                id="gridRadios1" value="Ya"
                                                                {{ $data->program == 'Ya' ? 'checked' : '' }}>
                                                            <label class="form-check-label" for="gridRadios1">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="program"
                                                                id="gridRadios1" value="Tidak"
                                                                {{ $data->program == 'Tidak' ? 'checked' : '' }}>
                                                            <label class="form-check-label" for="gridRadios1">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <button type="submit"
                                                class="md-btn md-raised m-b-sm w-xs blue text-decoration-none updateActivity"
                                                role="button">PERBARUI</button>
                                            {{-- <button type="reset" role="button"
                                                class="md-btn md-raised m-b-sm w-xs orange text-decoration-none text-white">RESET</button> --}}
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @elseif($page == 'create')
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <a href="{{ route('activity.index') }}"
                                    class="md-btn md-raised m-b-sm w-xs blue text-decoration-none text-white"
                                    role="button">View</a>
                            </div>
                            <div class="box-body">
                                <h5>Form Entry</h5>
                                <form id="addActivity" action="{{ route('activity.store') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="pak_id" value="{{ session()->get('pak_id') }}">
                                    <div class="row">
                                        <div class="col-md -col-sm col-lg">
                                            <label for="rek">NOMOR REKENING</label>
                                            <small class="text-danger">Mohon diisi dengan lengkap dan benar</small>
                                            <input type="text" class="form-control" id="rek" name="rek"
                                                placeholder="Contoh: 4.01.03.20.2.05.01" required autofocus>
                                        </div>
                                        <div class="col-md -col-sm col-lg">
                                            <label for="dana">SUMBER DANA</label>
                                            <select class="form-control" id="dana" name="dana" required>
                                                <option value="">---PILIH SUMBER DANA---</option>
                                                <option value="APBD Kabupaten Pamekasan">APBD Kabupaten Pamekasan</option>
                                                <option value="APBD Provinsi">APBD Provinsi</option>
                                                <option value="APBN">APBN</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md col-sm col-lg">
                                            <label for="subkegiatan">NAMA SUB KEGIATAN</label>
                                            <input type="text" class="form-control" name="subkegiatan" required>
                                        </div>
                                        <div class="col-md col-sm col-lg">
                                            <label for="pengadaan">JENIS PENGADAAN</label>
                                            <select class="form-control" name="pengadaan" id="pengadaan"
                                                onchange="select()" required>
                                                <option value="">---PILIH JENIS PENGADAAN---</option>
                                                <option value="Konstruksi">Konstruksi</option>
                                                <option value="Barang">Barang</option>
                                                <option value="Konsultasi">Konsultasi</option>
                                                <option value="Jasa Lainnya">Jasa Lainnya</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md col-sm col-lg">
                                            <label for="anggaran">ANGGARAN</label>
                                            <small class="text-info">Ditulis tanpa titik dan koma</small>
                                            <input type="text" class="form-control" name="anggaran" required>
                                        </div>
                                        <div class="col-md col-sm col-lg">
                                            <label for="pelaksanaan">METODE PELAKSANAAN</label>
                                            <select class="form-control" name="pelaksanaan" id="pelaksanaan" required>
                                                <option value="">---PILIH METODE PELAKSANAAN---</option>
                                                <option id="opt" value="Tender">Tender</option>
                                                <option value="Penunjukan Langsung">Penunjukan Langsung</option>
                                                <option value="Pengadaan Langsung">Pengadaan Langsung</option>
                                                <option value="ePurchasing">ePurchasing</option>
                                                <option value="Swakelola">Swakelola</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col">
                                            <label for="kegiatan">BENTUK KEGIATAN</label>
                                            <input type="text" class="form-control" name="kegiatan" required>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="laporan">LAPORAN</label>
                                                    <div class="lpError"></div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="gridCheck"
                                                                name="laporan[]" value="DAU">
                                                            <label class="form-check-label" for="gridCheck">
                                                                DAU
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="gridCheck"
                                                                name="laporan[]" value="DAK">
                                                            <label class="form-check-label" for="gridCheck">
                                                                DAK
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="gridCheck"
                                                                name="laporan[]" value="DBHC">
                                                            <label class="form-check-label" for="gridCheck">
                                                                DBHC
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="row">
                                                <div class="col">
                                                    <label for="program">PROGRAM PRIORITAS BUPATI</label>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="program"
                                                                id="gridRadios1" value="Ya" checked>
                                                            <label class="form-check-label" for="gridRadios1">
                                                                Ya
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="program"
                                                                id="gridRadios1" value="Tidak" checked>
                                                            <label class="form-check-label" for="gridRadios1">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <button type="submit"
                                                class="md-btn md-raised m-b-sm w-xs blue text-decoration-none"
                                                role="button">SIMPAN</button>
                                            <button type="reset"
                                                class="md-btn md-raised m-b-sm w-xs orange text-decoration-none text-white"
                                                role="button">RESET</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <!-- ############ PAGE END-->
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function() {
            $("#table").DataTable();
        });

        $(document).ready(function() {
            $(".deleteActivity").click(function(e) {
                e.preventDefault();
                var _token = $("input[name='_token']").val();
                var Url = $(this).parents('form').attr('action');
                $.ajax({
                    type: 'DELETE',
                    url: Url,
                    data: {
                        _token: _token
                    },
                    success: function(data) {
                        if ($.isEmptyObject(data.error)) {
                            window.setTimeout(function() {
                                location.reload();
                            }, 1000);
                        } else {
                            printErrorMsg(data.error);
                        }
                    }
                });
            });
        });

        $(document).ready(function() {
            $(".updateActivity").click(function(e) {
                e.preventDefault();
                var _token = $("input[name='_token']").val();
                var id = $("input[name='id']").val();
                var rek = $("input[name='rek']").val();
                var dana = document.getElementById("dana").value;
                var subkegiatan = $("input[name='subkegiatan']").val();
                var pengadaan = document.getElementById("pengadaan").value;
                var anggaran = $("input[name='anggaran']").val();
                var pelaksanaan = document.getElementById("pelaksanaan").value;
                var kegiatan = $("input[name='kegiatan']").val();
                var laporan = $("input[name='laporan']").val();
                var Url = $(this).parents('form').attr('action');
                console.log(Url);

                $.ajax({
                    type: 'POST',
                    url: Url,
                    data: {
                        _token: _token,
                        id: id,
                        rek: rek,
                        dana: dana,
                        subkegiatan: subkegiatan,
                        pengadaan: pengadaan,
                        anggaran: anggaran,
                        pelaksanaan: pelaksanaan,
                        kegiatan: kegiatan
                    },
                    success: function(data) {
                        if ($.isEmptyObject(data.error)) {
                            window.location.href = '{{ route('activity.index') }}'
                        } else {
                            printErrorMsg(data.error);
                        }
                    }
                });
            });
        });

        $.validator.setDefaults({
            submitHandler: function() {
                $(form).submit();
            }
        });

        $(document).ready(function() {
            $("#addActivity").validate({
                rules: {
                    subkegiatan: "required",
                    anggaran: {
                        required: true,
                        minlength: 7
                    },
                    kegiatan: "required",
                    rek: {
                        required: true,
                        minlength: 5
                    },
                    "laporan[]": {
                        required: true,
                        minlength: 1
                    }
                },
                messages: {
                    rek: {
                        minlength: "NOMOR REKENIK minimal 5 digit!"
                    },
                    anggaran: {
                        minlength: "Minimal anggaran 1.000.000!"
                    },
                    "laporan[]": "Harap pilih min 1 laporan!"
                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    let lp = document.getElementsByClassName("lpError");

                    if (element.prop("type") === "checkbox") {
                        error.append(lp);
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                }
            });

        });

        function select() {
            var select = document.getElementById('pengadaan');
            var option = select.options[select.selectedIndex];
            // console.log(option.value);

            if (option.value == "Konsultasi") {
                $('#opt').replaceWith(`<option value="Seleksi" >
                                       Seleksi
                                  </option>`);
            }
        }
        select();
    </script>
@endpush
