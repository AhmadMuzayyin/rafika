<div class="page">
    <form id="formID" action="{{ route('target.store') }}">
        @csrf
        <div class="row mb-3">
            <div class="col-md col-sm col-lg">
                <label for="kegiatan">TARGET KEGIATAN</label>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" id="kegiatan" role="progressbar"
                        aria-valuenow="{{ !$k->isEmpty() ? $k[0]->progres : '' }}" aria-valuemin="0"
                        aria-valuemax="100" style="width: {{ !$k->isEmpty() ? $k[0]->progres : '' }}%">
                        @if ($k->isEmpty())
                        @else
                            Complete {{ $k[0]->progres == null ? '' : $k[0]->progres }}%
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="row mt-2">
                        <input type="hidden" name="target" id="target" value="{{ $data->activity->anggaran }}">
                        <input type="hidden" name="id" value="{{ $data->id }}">
                        <input type="hidden" name="ac" value="{{ $ac->activity_id }}">
                        <input type="hidden" name="pak" value="{{ $ac->activity->pak_id }}">
                        <div class="col">
                            <div class="row">
                                <div class="col">
                                    <label for="januari">JANUARI</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="11" name="AC_januari" required autofocus
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[0]->anggaran) : '' }}">
                                </div>
                                <div class="col">
                                    <label for="februari">FEBRUARI</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="12" name="AC_februari" required
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[1]->anggaran) : '' }}">
                                </div>
                                <div class="col">
                                    <label for="marete">MARET</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="13" name="AC_maret" required
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[2]->anggaran) : '' }}">
                                </div>
                                <div class="col">
                                    <label for="april">APRIL</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="14" name="AC_april" required
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[3]->anggaran) : '' }}">
                                </div>
                                <div class="col">
                                    <label for="mei">MEI</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="15" name="AC_mei" required
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[4]->anggaran) : '' }}">
                                </div>
                                <div class="col">
                                    <label for="juni">JUNI</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="16" name="AC_juni" required
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[5]->anggaran) : '' }}">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <label for="juli">JULI</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="17" name="AC_juli" required
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[6]->anggaran) : '' }}">
                                </div>
                                <div class="col">
                                    <label for="agustus">AGUSTUS</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="18" name="AC_agustus" required
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[7]->anggaran) : '' }}">
                                </div>
                                <div class="col">
                                    <label for="september">SEPREMBER</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="19" name="AC_september" required
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[8]->anggaran) : '' }}">
                                </div>
                                <div class="col">
                                    <label for="oktober">OKTOBER</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="20" name="AC_oktober" required
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[9]->anggaran) : '' }}">
                                </div>
                                <div class="col">
                                    <label for="november">NOVEMBER</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="21" name="AC_november" required
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[10]->anggaran) : '' }}">
                                </div>
                                <div class="col">
                                    <label for="desember">DESEMBER</label>
                                    <input type="{{ $k == null ? 'number' : 'text' }}" class="form-control"
                                        style="font-size: 85%" id="22" name="AC_desember" required
                                        value="{{ !$k->isEmpty() ? \FormatUang::format($k[11]->anggaran) : '' }}">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @include('admin.entry.page.keuangan')
        <div class="row mt-2">
            <div class="col">
                <button type="submit" class="md-btn md-raised m-b-sm blue addTarget" role="button">SIMPAN</button>
            </div>
        </div>
    </form>
</div>

@push('script')
    <script>
        var jan, feb, mar, apr, mei, jun, jul, agus, sep, okt, nov, des;
        var target = $("input[name='target']").val();
        $('#11').keyup(function() {
            jan = ($(this).val());
            document.getElementById("1").value = getPersen(jan);
        });
        $('#12').keyup(function() {
            feb = ($(this).val());
            document.getElementById("2").value = getPersen(feb);
            document.getElementById("total").value = formatRupiah($(this).val());
        });
        $('#13').keyup(function() {
            mar = ($(this).val());
            document.getElementById("3").value = getPersen(mar);
        });
        $('#14').keyup(function() {
            apr = ($(this).val());
            document.getElementById("4").value = getPersen(apr);
        });
        $('#15').keyup(function() {
            mei = ($(this).val());
            document.getElementById("5").value = getPersen(mei);
        });
        $('#16').keyup(function() {
            jun = ($(this).val());
            document.getElementById("6").value = getPersen(jun);
        });
        $('#17').keyup(function() {
            jul = ($(this).val());
            document.getElementById("7").value = getPersen(jul);
        });
        $('#18').keyup(function() {
            agus = ($(this).val());
            document.getElementById("8").value = getPersen(agus);
        });
        $('#19').keyup(function() {
            sep = ($(this).val());
            document.getElementById("9").value = getPersen(sep);
        });
        $('#20').keyup(function() {
            okt = ($(this).val());
            document.getElementById("10").value = getPersen(okt);
        });
        $('#21').keyup(function() {
            nov = ($(this).val());
            document.getElementById("011").value = getPersen(nov);
        });
        $('#22').keyup(function() {
            des = ($(this).val());
            document.getElementById("012").value = getPersen(des);
        });

        function getPersen(value) {

            persen = (value / target) * 100;
            return persen
        }

        $(document).ready(function() {
            $(".addTarget").click(function(e) {
                e.preventDefault();
                var _token = $("input[name='_token']").val();
                var id = $("input[name='id']").val();

                var AC_januari = $("input[name='AC_januari']").val();
                var AC_februari = $("input[name='AC_februari']").val();
                var AC_maret = $("input[name='AC_maret']").val();
                var AC_april = $("input[name='AC_april']").val();
                var AC_mei = $("input[name='AC_mei']").val();
                var AC_juni = $("input[name='AC_juni']").val();
                var AC_juli = $("input[name='AC_juli']").val();
                var AC_agustus = $("input[name='AC_agustus']").val();
                var AC_september = $("input[name='AC_september']").val();
                var AC_oktober = $("input[name='AC_oktober']").val();
                var AC_november = $("input[name='AC_november']").val();
                var AC_desember = $("input[name='AC_desember']").val();

                var januari = $("input[name='januari']").val();
                var februari = $("input[name='februari']").val();
                var maret = $("input[name='maret']").val();
                var april = $("input[name='april']").val();
                var mei = $("input[name='mei']").val();
                var juni = $("input[name='juni']").val();
                var juli = $("input[name='juli']").val();
                var agustus = $("input[name='agustus']").val();
                var september = $("input[name='september']").val();
                var oktober = $("input[name='oktober']").val();
                var november = $("input[name='id']").val();
                var desember = $("input[name='desember']").val();
                var ac = $("input[name='ac']").val();

                let keuangan = {
                    "januari": AC_januari,
                    "februari": AC_februari,
                    "maret": AC_maret,
                    "april": AC_april,
                    "mei": AC_mei,
                    "juni": AC_juni,
                    "juli": AC_juli,
                    "agustus": AC_agustus,
                    "september": AC_september,
                    "oktober": AC_oktober,
                    "november": AC_november,
                    "desember": AC_desember,
                };

                let target = {
                    "januari": januari,
                    "februari": februari,
                    "maret": maret,
                    "april": april,
                    "mei": mei,
                    "juni": juni,
                    "juli": juli,
                    "agustus": agustus,
                    "september": september,
                    "oktober": oktober,
                    "november": november,
                    "desember": desember,
                };

                var Url = $(this).parents('form').attr('action');
                var cek = parseInt(AC_januari) + parseInt(AC_februari) + parseInt(AC_maret) + parseInt(
                        AC_april) +
                    parseInt(AC_mei) + parseInt(AC_juni) + parseInt(AC_juli) +
                    parseInt(AC_agustus) + parseInt(AC_september) + parseInt(AC_oktober) + parseInt(
                        AC_november) +
                    parseInt(AC_desember);
                if (cek > target) {
                    alert("Melebihi maksimal target")
                } else if (cek < target) {
                    alert("Tidak Boleh kurang dari target")
                } else {
                    $.ajax({
                        type: 'POST',
                        url: Url,
                        data: {
                            _token: _token,
                            id: id,
                            ac: ac,
                            target: Object.entries(target),
                            keuangan: Object.entries(keuangan)

                        },
                        success: function(data) {
                            if ($.isEmptyObject(data.error)) {
                                window.setTimeout(function() {
                                    location.reload();
                                }, 1000);
                            } else {
                                printErrorMsg(data.error);
                            }
                        }
                    });
                }

            });
        });

        $(document).ready(function() {
            $(".deleteActivity").click(function(e) {
                e.preventDefault();
                var _token = $("input[name='_token']").val();
                var id = $("input[name='id']").val();
                var Url = $(this).parents('form').attr('action');
                $.ajax({
                    type: 'DELETE',
                    url: Url,
                    data: {
                        _token: _token
                    },
                    success: function(data) {
                        if ($.isEmptyObject(data.error)) {
                            window.setTimeout(function() {
                                location.reload();
                            }, 1000);
                        } else {
                            printErrorMsg(data.error);
                        }
                    }
                });
            });
        });
    </script>

    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }

    </style>
@endpush
