@extends('template.main')


@section('content')
    <div ui-view class="app-body" id="view">
        <!-- ############ PAGE START-->
        <div class="padding">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1>DATA [ARSIP LAPORAN RFK]</h1>
                            {{-- @dd($item) --}}
                            <form action="">
                                @csrf
                                <div class="row">
                                    <div class="col-md col-sm col-lg">
                                        <select class="custom-select form-control" id="dana" name="dana">
                                            <option value="APBD Kabupaten Pamekasan"
                                                {{ $item == 'APBD Kabupaten Pamekasan' ? 'selected' : '' }}>APBD
                                                Kabupaten Pamekasan</option>
                                            <option value="APBD Provinsi"
                                                {{ $item == 'APBD Provinsi' ? 'selected' : '' }}>APBD Provinsi
                                            </option>
                                            <option value="APBN" {{ $item == 'APBN' ? 'selected' : '' }}>APBN
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table" id="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>BULAN</th>
                                            <th>COVER</th>
                                            <th>JADWAL</th>
                                            <th>RFK</th>
                                            <th>GRAFIK</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->bulan }}</td>
                                                <td>
                                                    <form action="{{ route('get.Cover') }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="bulan" value="{{ $item->bulan }}">
                                                        <input type="hidden" name="pak" value="{{ $item->pak_id }}">
                                                        <input type="hidden" name="user" value="{{ $item->user_id }}">
                                                        <button type="submit"
                                                            class="md-btn md-raised m-b-sm w-xs blue text-decoration-none text-white"
                                                            role="button">COVER</button>
                                                    </form>
                                                </td>
                                                <td>
                                                    <form action="{{ route('get.Jadwal') }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="bulan" value="{{ $item->bulan }}">
                                                        <input type="hidden" name="pak" value="{{ $item->pak_id }}">
                                                        <input type="hidden" name="user" value="{{ $item->user_id }}">
                                                        <input type="hidden" name="ac" value="{{ $item->activity_id }}">
                                                        <button type="submit"
                                                            class="md-btn md-raised m-b-sm w-xs blue text-decoration-none text-white"
                                                            role="button">JADWAL</button>
                                                    </form>
                                                </td>
                                                <td>
                                                    <a href="#"
                                                        class="md-btn md-raised m-b-sm w-xs blue text-decoration-none text-white"
                                                        role="button">RFK</a>
                                                </td>
                                                <td>
                                                    <a href="#"
                                                        class="md-btn md-raised m-b-sm w-xs blue text-decoration-none text-white"
                                                        role="button">GRAFIK</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ############ PAGE END-->
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $("#table").DataTable();
        });
        $(document).ready(function() {
            $("#dana").change(function(e) {
                e.preventDefault();
                var _token = $("input[name='_token']").val();
                var dana = $(this).children("option:selected").val();
                // console.log(id);
                window.location.href = '{{ url('arsip') }}?data=' + dana
                $.ajax({
                    type: 'POST',
                    url: '{{ route('get.arsip') }}',
                    data: {
                        _token: _token,
                        dana: dana,
                    },
                    success: function(data) {
                        if ($.isEmptyObject(data.error)) {
                            window.location.href = '{{ url('arsip') }}?data=' + data.selected
                        } else {
                            printErrorMsg(data.error);
                        }
                    }
                });
            });
        });
    </script>
@endpush
