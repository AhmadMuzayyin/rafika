<table class="table table-bordered mt-2">
    <thead>
        <tr>
            <th class="tg-0pky" rowspan="2">NO</th>
            <th class="tg-0pky" rowspan="2">NAMA SUB KEGIATAN</th>
            <th class="tg-0pky" rowspan="2">ANGGARAN (Rp.)</th>
            <th class="tg-0pky" rowspan="2">BOBOT</th>
            <th class="tg-0pky" rowspan="2">VOLUME</th>
            <th class="tg-0pky" rowspan="2">LOKASI</th>
            <th class="tg-0pky" rowspan="2">PPTK</th>
            <th class="tg-0pky">JENIS PENGADAAN</th>
            <th class="tg-0pky" colspan="2">REALISASI KEGIATAN</th>
            <th class="tg-0pky" colspan="4">REALISASI KEUANGAN</th>
        </tr>
        <tr>
            <th class="tg-0pky">TARGET</th>
            <th class="tg-0pky">LAPORAN</th>
            <th class="tg-0pky">TARGET</th>
            <th class="tg-0pky">LAPORAN</th>
            <th class="tg-0pky">LAPORAN</th>
            <th class="tg-0pky">LAPORAN</th>
            <th class="tg-0pky">SISA</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
            <td class="tg-0lax"></td>
        </tr>
        <tr>
            <td class="tg-0pky" colspan="2">Total</td>
            <td class="tg-0pky"></td>
            <td class="tg-0pky"></td>
            <td class="tg-0pky"></td>
            <td class="tg-0pky"></td>
            <td class="tg-0pky"></td>
            <td class="tg-0pky"></td>
            <td class="tg-0pky"></td>
            <td class="tg-0pky"></td>
            <td class="tg-0pky"></td>
            <td class="tg-0pky"></td>
            <td class="tg-0pky"></td>
            <td class="tg-0pky"></td>
        </tr>
    </tbody>
</table>
