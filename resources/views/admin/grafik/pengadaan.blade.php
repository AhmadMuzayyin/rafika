@extends('template.main')


@section('content')
    <div ui-view class="app-body" id="view">
        <!-- ############ PAGE START-->
        <canvas id="myChart" style='width:10px !important;height:10px !important'></canvas>
        <!-- ############ PAGE END-->
    </div>
@endsection

@push('script')
<script>

    $(document).ready(function () {
        $.ajax({
            type: "GET",
            url: "{{ route('pengadaan-data') }}",
            success: function (res) {
                
                let label = []
                let datanya = []
                $.each(res.data, function (key, val) { 

                    label.push(key)
                    datanya.push(val.length)

                });

                const ctx = document.getElementById('myChart').getContext('2d');
                ctx.canvas.parentNode.style.height = '500px';
                ctx.canvas.parentNode.style.width = '500px';
    
                const data = {
                    labels: label,
                    datasets: [{
                        label: 'My First Dataset',
                        data: datanya,
                        backgroundColor: [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        ],
                        hoverOffset: 3
                    }]
                };
                const myChart = new Chart(ctx, {
                    type: 'pie',
                    data: data,
                });
            }
        });
    });

    
</script>
@endpush
