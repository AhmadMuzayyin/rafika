@extends('template.main')


@section('content')
    <div ui-view class="app-body" id="view">
        <!-- ############ PAGE START-->
        <div class="padding">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h1>DATA [REKAPITULASI]</h1>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <th rowspan="3">No</th>
                                            <th rowspan="3">OPD</th>
                                            <th rowspan="3">JUMLAH PAKET</th>
                                            <th rowspan="3">ANGGARAN(Rp.)</th>
                                            <th colspan="2">KEGIATAN</th>
                                            <th colspan="3">KEUANGAN</th>
                                            <th rowspan="3">REALISASI</th>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">TARGET</th>
                                            <th rowspan="2">REALISASI</th>
                                            <th rowspan="2">TARGET(%)</th>
                                            <th colspan="2">KETERANGAN</th>
                                        </tr>
                                        <tr>
                                            <th>Rp.</th>
                                            <th>%</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>BAGIAN ADMINISTRASI PEMBANGUNAN</td>
                                            <td>1</td>
                                            <td>100000000</td>
                                            <td>100</td>
                                            <td>90</td>
                                            <td>100</td>
                                            <td>90000000</td>
                                            <td>90</td>
                                            <td>
                                                MELAPOR
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><strong>TOTAL</strong></td>
                                            <td><strong>1</strong></td>
                                            <td><strong>100000000</strong></td>
                                            <td><strong>100</strong></td>
                                            <td><strong>90</strong></td>
                                            <td><strong>100</strong></td>
                                            <td><strong>90000000</strong></td>
                                            <td colspan="2"><strong>90</strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ############ PAGE END-->
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $("#table").DataTable();
        });
    </script>
@endpush
