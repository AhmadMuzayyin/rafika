<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ url('assets/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />
    <title>COVER</title>

    <style>
        .text-center,
        h3 {
            text-align: center
        }

    </style>
</head>

<body>
    {{-- @dd($target) --}}
    @foreach ($data as $item)
        <div class="text-center">
            <h3>JADWAL KEGIATAN BELANJA LANGSUNG</h3>
            <h3>SUMBER DANA {{ strtoupper($item->dana) }}</h3>
            <h3> TAHUN ANGGARAN {{ $pak->nama }}</h3>
            <h3>BAGIAN {{ strtoupper($jb->nama_SKPD) }}</h3>
            <hr>
            {{-- {{ $item }} --}}
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th rowspan="2">NO</th>
                    <th rowspan="2">NAMA SUB KEGIATAN</th>
                    <th rowspan="2">ANGGARAN(RP.)</th>
                    <th rowspan="2">LOKASI</th>
                    <th rowspan="2">PPTK</th>
                    <th colspan="12">Target Kegiatan</th>
                    <th rowspan="2">PARAF PPTK</th>
                </tr>
                <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>PENGELOLAAN EVALUASI PEMBANGUNAN</td>
                    <td>100000000</td>
                    <td>Bukek, Tlanakan Kabupaten Pamekasan</td>
                    <td>AHMAD MUZAYYIN</td>

                    {{-- @foreach ($target as $item)
                        <td>{{ $item->januari }}</td>
                        <td>{{ $item->februari }}</td>
                        <td>{{ $item->maret }}</td>
                        <td>{{ $item->april }}</td>
                        <td>{{ $item->mei }}</td>
                        <td>{{ $item->juni }}</td>
                        <td>{{ $item->juli }}</td>
                        <td>{{ $item->agustus }}</td>
                        <td>{{ $item->september }}</td>
                        <td>{{ $item->oktober }}</td>
                        <td>{{ $item->november }}</td>
                        <td>{{ $item->desember }}</td>
                    @endforeach --}}
                    <td>10</td>
                    <td>10</td>
                    <td>10</td>
                    <td>10</td>
                    <td>10</td>
                    <td>10</td>
                    <td>10</td>
                    <td>10</td>
                    <td>10</td>
                    <td>10</td>
                    <td>10</td>
                    <td>10</td>

                    <td></td>
                </tr>
                <tr>
                    <td colspan="18">
                        <p><strong>Jumlah Kegiatan : 1</strong></p>
                        <p><strong>Total Anggaran : 100000000</strong></p>
                    </td>
                </tr>
            </tbody>
        </table>
    @endforeach
</body>
<script>
    // window.print();
</script>

</html>
