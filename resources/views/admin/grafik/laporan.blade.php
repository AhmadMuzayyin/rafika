@extends('template.main')


@section('content')
    <div ui-view class="app-body" id="view">
        <!-- ############ PAGE START-->
        <div class="padding">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3>Basic Pie</h3>
                            <small class="block text-muted">set center, radius</small>
                        </div>
                        <div class="box-body">
                            <div ui-jp="chart" ui-options=" {
                          tooltip : {
                              trigger: 'item',
                              formatter: '{a} <br/>{b} : {c} ({d}%)'
                          },
                          legend: {
                              orient : 'vertical',
                              x : 'left',
                              data:['Direct','Mail','Affiliate','AD','Search']
                          },
                          calculable : true,
                          series : [
                              {
                                  name:'Source',
                                  type:'pie',
                                  radius : '55%',
                                  center: ['50%', '60%'],
                                  data:[
                                      {value:335, name:'Direct'},
                                      {value:310, name:'Mail'},
                                      {value:234, name:'Affiliate'},
                                      {value:135, name:'AD'},
                                      {value:1548, name:'Search'}
                                  ]
                              }
                          ]
                        }" style="height: 300px; -webkit-tap-highlight-color: transparent; user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;"
                                _echarts_instance_="1643736695204">
                                <div style="position: relative; overflow: hidden; width: 364px; height: 300px;">
                                    <div data-zr-dom-id="bg" class="zr-element"
                                        style="position: absolute; left: 0px; top: 0px; width: 507px; height: 300px; user-select: none;">
                                    </div><canvas width="364" height="300" data-zr-dom-id="0" class="zr-element"
                                        style="position: absolute; left: 0px; top: 0px; width: 364px; height: 300px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></canvas><canvas
                                        width="364" height="300" data-zr-dom-id="1" class="zr-element"
                                        style="position: absolute; left: 0px; top: 0px; width: 364px; height: 300px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></canvas><canvas
                                        width="364" height="300" data-zr-dom-id="_zrender_hover_" class="zr-element"
                                        style="position: absolute; left: 0px; top: 0px; width: 364px; height: 300px; user-select: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></canvas>
                                    <div class="echarts-tooltip zr-element"
                                        style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgb(0, 0, 0); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); font-family: Arial, Verdana, sans-serif; padding: 10px 15px; left: 335px; top: 152px;">
                                        Source <br>Mail : 310 (12.10%)</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ############ PAGE END-->
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $("#table").DataTable();
        });
    </script>
@endpush
