<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>COVER</title>

    <style>
        .text-center,
        h3 {
            text-align: center
        }

    </style>
</head>

<body>
    {{-- @dd($jml) --}}
    @foreach ($data as $item)
        <div class="text-center">
            <h3>HASIL EVALUASI {{ strtoupper($item->dana) }}</h3>
            <h3>{{ strtoupper($item->bulan) }} TAHUN ANGGARA {{ $pak->nama }}</h3>
            <h3>BAGIAN {{ strtoupper($jb->nama_SKPD) }}</h3>
            <hr>
            {{-- {{ $item }} --}}
        </div>
        <ol>
            <li>Jumlah Paket Sub Kegiatan : {{ $jml }}</li>
            <li>Jumlah Anggaran : {{ $item->anggaran }}</li>
            <li>Realisasi Keuangan s/d Bulan ini : 0</li>
            <li>Rata-rata Realisasi Fisik Sub Kegiatan : {{ $rfk }}</li>
        </ol>
    @endforeach
</body>
<script>
    // window.print();
</script>

</html>
