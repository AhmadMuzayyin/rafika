@extends('template.main')

@section('content')
    <div ui-view class="app-body" id="view">
        <!-- ############ PAGE START-->
        {{-- @dd($pak_id != null ? $pak_id : '') --}}
        <div class="p-a white lt box-shadow">
            <div class="row">
                <div class="col-sm-6">
                    <h4 class="mb-0 _300">Welcome to RAFIKA</h4>
                    <small class="text-muted">Application for REALISASI FISIK DAN KEUANGAN</small>
                </div>
            </div>
        </div>
        <div class="padding">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="box p-a">
                        <div class="pull-left m-r">
                            <span class="w-48 rounded  accent">
                                <i class="material-icons">&#xe151;</i>
                            </span>
                        </div>
                        <div class="clear">
                            <h4 class="m-0 text-lg _300"><a href>125 <span class="text-sm">Emails</span></a></h4>
                            <small class="text-muted">6 new arrivals.</small>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="box p-a">
                        <div class="pull-left m-r">
                            <span class="w-48 rounded primary">
                                <i class="material-icons">&#xe54f;</i>
                            </span>
                        </div>
                        <div class="clear">
                            <h4 class="m-0 text-lg _300"><a href>40 <span class="text-sm">Projects</span></a>
                            </h4>
                            <small class="text-muted">38 open.</small>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="box p-a">
                        <div class="pull-left m-r">
                            <span class="w-48 rounded warn">
                                <i class="material-icons">&#xe8d3;</i>
                            </span>
                        </div>
                        <div class="clear">
                            <h4 class="m-0 text-lg _300"><a href>600 <span class="text-sm">Users</span></a>
                            </h4>
                            <small class="text-muted">632 vips.</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3>Progress Keuangan</h3>
                        </div>
                        <div class="box-body">
                            <canvas id="myChart" style='width:10px !important;height:10px !important'></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ############ PAGE END-->

    </div>
@endsection

@push('script')
<script>

    function titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }
        // Directly return the joined string
        return splitStr.join(' '); 
    }

    $(document).ready(function () {
        $.ajax({
            type: "GET",
            url: "{{ route('dashboard-data') }}",
            success: function (res) {
                
                let label = []
                let datanya = []
                $.each(res.data, function (key, val) { 
                    label.push(titleCase(key))
                    datanya.push(val.length)

                });

                const ctx = document.getElementById('myChart').getContext('2d');
    
                const labels = label;

                const data = {
                    labels: labels,
                    datasets: [{
                    label: 'My First dataset',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: datanya,
                    }]
                };

                const config = {
                    type: 'line',
                    data: data,
                    options: {}
                };

                const myChart = new Chart(ctx, config);

                
            }
        });
    });

    

    
</script>
@endpush
