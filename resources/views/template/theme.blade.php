<!-- theme switcher -->
<div id="switcher">
    <div class="switcher box-color dark-white text-color" id="sw-theme">
        <a href ui-toggle-class="active" target="#sw-theme" class="box-color dark-white text-color sw-btn">
            <i class="fa fa-gear"></i>
        </a>
        <div class="box-header">
            <h2>Theme Switcher</h2>
        </div>
        <div class="box-divider"></div>
        <div class="box-body">
            <div data-target="bg" class="row no-gutter text-u-c text-center _600 clearfix">
                <label class="p-a col-sm-6 light pointer m-0">
                    <input type="radio" name="theme" value="" hidden>
                    Light
                </label>
                <label class="p-a col-sm-6 grey pointer m-0">
                    <input type="radio" name="theme" value="grey" hidden>
                    Grey
                </label>
                <label class="p-a col-sm-6 dark pointer m-0">
                    <input type="radio" name="theme" value="dark" hidden>
                    Dark
                </label>
                <label class="p-a col-sm-6 black pointer m-0">
                    <input type="radio" name="theme" value="black" hidden>
                    Black
                </label>
            </div>
        </div>
    </div>

    <div class="switcher box-color black lt" id="sw-demo">
        <div class="box-header">
            <h2>Demos</h2>
        </div>
        <div class="box-divider"></div>
        <div class="box-body">
            <div class="row no-gutter text-u-c text-center _600 clearfix">
                <a href="dashboard.html" class="p-a col-sm-6 primary">
                    <span class="text-white">Default</span>
                </a>
                <a href="dashboard.0.html" class="p-a col-sm-6 success">
                    <span class="text-white">Zero</span>
                </a>
                <a href="dashboard.1.html" class="p-a col-sm-6 blue">
                    <span class="text-white">One</span>
                </a>
                <a href="dashboard.2.html" class="p-a col-sm-6 warn">
                    <span class="text-white">Two</span>
                </a>
                <a href="dashboard.3.html" class="p-a col-sm-6 danger">
                    <span class="text-white">Three</span>
                </a>
                <a href="dashboard.4.html" class="p-a col-sm-6 green">
                    <span class="text-white">Four</span>
                </a>
                <a href="dashboard.5.html" class="p-a col-sm-6 info">
                    <span class="text-white">Five</span>
                </a>
                <div class="p-a col-sm-6 lter">
                    <span class="text">...</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End theme switcher -->
