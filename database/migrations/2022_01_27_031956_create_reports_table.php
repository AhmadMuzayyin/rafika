<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->foreignId('activity_id');
            $table->foreignId('user_id');
            $table->foreignId('pak_id');
            $table->string('bulan')->nullable();
            $table->string('anggaran')->nullable();
            $table->string('kegiatan')->nullable();
            $table->string('dana')->nullable();
            $table->string('kegiatan_lalu')->nullable();
            $table->string('kegiatan_sekarang')->nullable();
            $table->string('keuangan_lalu')->nullable();
            $table->string('keuangan_sekarang')->nullable();
            $table->string('kendala')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
