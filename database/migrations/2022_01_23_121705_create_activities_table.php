<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pak_id');
            $table->foreignId('user_id');
            $table->string('rek');
            $table->string('nama');
            $table->integer('anggaran');
            $table->string('pengadaan');
            $table->string('pelaksanaan');
            $table->string('dana');
            $table->string('laporan');
            $table->string('kegiatan');
            $table->string('program');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
