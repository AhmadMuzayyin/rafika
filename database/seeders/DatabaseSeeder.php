<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::create([
            'kode_SKPD' => '1.1.1',
            'nama_SKPD' => 'admin',
            'nama_operator' => 'Ahmad Muzayyin',
            'no_hp' => '123456789',
            'no_kantor' => '4321',
            'alamat_kantor' => 'Pamekasan',
            'username' => 'admin',
            'password' => bcrypt('12345'),
            'nama_KPA' => 'Ahmad Muzayyin',
            'foto' => 'admin.jpg',
            'isAdmin' => true
        ]);
        User::create([
            'kode_SKPD' => '1.1.2',
            'nama_SKPD' => 'ADMINISTRASI PEMBANGUNAN',
            'nama_operator' => 'Ahmad Muzayyin',
            'no_hp' => '123456789',
            'no_kantor' => '4321',
            'alamat_kantor' => 'Pamekasan',
            'username' => 'user',
            'password' => bcrypt('12345'),
            'nama_KPA' => 'Ahmad Muzayyin',
            'foto' => '',
            'isAdmin' => false
        ]);
    }
}
