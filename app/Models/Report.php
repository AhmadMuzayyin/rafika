<?php

namespace App\Models;

use App\Models\Pak;
use App\Models\Activity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Report extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function pak(){
        return $this->belongsTo(Pak::class);
    }

    public function activity(){
        return $this->belongsTo(Activity::class);
    }
}
