<?php
namespace App\Helpers;

class FormatUang{

  public static function format($expression){
    return number_format($expression,0,',','.');
  }
  
}