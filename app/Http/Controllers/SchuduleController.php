<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\PPTK;
use App\Models\Schedule;
use App\Models\Target;
use App\Models\TKeuangan;
use App\Models\UserPPTK;
use App\Models\Volume;
use Illuminate\Http\Request;

class SchuduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.entry.schedule',
        [
            'page' => 'index',
            'data' => Schedule::all()
        ]);
    }

    public function edit(Schedule $schedule)
    {
        return view('admin.entry.schedule', [
            'page' => 'lokasi',
            'data' => $schedule,
            'lokasi' => Location::where('schedule_id', $schedule->id)->get()
        ]);
    }

    public function volume(Schedule $schedule)
    {
        return view('admin.entry.schedule', [
            'page' => 'volume',
            'data' => $schedule,
            'volume' => Volume::firstWhere('schedule_id', $schedule->id)
        ]);
    }
    public function pptk(Schedule $schedule)
    {
        return view('admin.entry.schedule', [
            'page' => 'PPTK',
            'data' => $schedule,
            'Userpptk' => UserPPTK::all(),
            'pptk' => PPTK::firstWhere('schedule_id', $schedule->id)
        ]);
    }
    public function target(Schedule $schedule)
    {
        $target = Target::where('schedule_id', $schedule->id)->get();
        $data = TKeuangan::where('schedule_id', $schedule->id)->get();

        return view('admin.entry.schedule', [
            'page' => 'target',
            'data' => $schedule,
            'target' => $target ? $target : [],
            'k' => $data ? $data : [],
            'ac' => Schedule::firstWhere('id', $schedule->id) 
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        //
    }
}
