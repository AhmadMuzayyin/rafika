<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        return view('admin.index');
    }

    public function indexData(){
        $data = Report::select('id', 'status', 'user_id', 'anggaran', 'bulan',)
        ->where('user_id', auth()->user()->id)
        ->where('status', 1)->get()->groupBy('bulan');
        
        return response()->json(['data' => $data]);
    }
}
