<?php

namespace App\Http\Controllers;

use App\Models\PPTK;
use App\Models\Schedule;
use Illuminate\Http\Request;

class PPTKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        try {
            $cek = PPTK::firstWhere('schedule_id', $request->id);
            if ($cek == true) {
                $p = PPTK::find($cek->id);
                $p->schedule_id = $request->id;
                $p->user_p_p_t_k_id = $request->pptk;
                $p->save();
            }else{
                $pptk = new PPTK;
                $pptk->schedule_id = $request->id;
                $pptk->user_p_p_t_k_id = $request->pptk;
                $pptk->save();

                $s = Schedule::find($request->id);
                if ($s == null) {
                    $s->persentase = 20;
                }
                $s->persentase = $s->persentase + 20;
                $s->save();
            }
            

            return redirect('/schedule/'. $request->id .'/edit/pptk')->with('success', 'Data berhasil ditambah!');
        } catch (\Throwable $th) {
            return response()->json(['tryError',$th->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PPTK  $pPTK
     * @return \Illuminate\Http\Response
     */
    public function show(PPTK $pPTK)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PPTK  $pPTK
     * @return \Illuminate\Http\Response
     */
    public function edit(PPTK $pPTK)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PPTK  $pPTK
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PPTK $pPTK)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PPTK  $pPTK
     * @return \Illuminate\Http\Response
     */
    public function destroy(PPTK $pPTK)
    {
        //
    }
}
