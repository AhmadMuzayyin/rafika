<?php

namespace App\Http\Controllers;

use App\Models\Pak;
use App\Models\User;
use App\Models\Grafik;
use App\Models\Report;
use App\Models\Activity;
use App\Models\Schedule;
use App\Models\Target;
use Illuminate\Http\Request;
use Illuminate\Notifications\Action;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class GrafikController extends Controller
{
    public function rekap()
    {
        $report = Report::where('user_id', Auth()->user()->id)->get('activity_id');
        // dd($report);
        foreach ($report as $asu) {
            $ag = DB::table('reports')->select(DB::raw('sum(anggaran) as ag, status'))->where('status', 1)->where('activity_id', $asu->activity_id)->groupBy('status')->value('ag');
            $jml = Activity::firstWhere('id', $asu->activity_id);
            $total = ($ag / $jml->anggaran) * 100;
            $id = $asu->activity_id;
        }
        // die();
        return view('admin.grafik.rekap', [
            'rel' => $total,
            'target' => Activity::where('id', $id)->get()
        ]);
    }

    public function arsip(Request $request)
    {
        if (request()->get('data') == null) {
            $selected = "APBD Kabupaten Pamekasan";
            return view('admin.grafik.arsip', [
                'data' => Report::where('status', 1)->where('dana', $selected)->get(),
                'item' => $selected
            ]);
        } else {
            return view('admin.grafik.arsip', [
                'data' => Report::where('status', 1)->where('dana', request()->get('data'))->get(),
                'item' => request()->get('data')
            ]);
        }
    }

    public function getArsip(Request $request)
    {
        return response()->json([
            'success' => 'Success',
            'selected' => $request->dana
        ]);
    }

    public function getCover(Request $request)
    {
        $ac = Report::where('bulan', $request->bulan)->where('status', 1)->get();
        foreach ($ac as $i) {
            $ac = $i->activity_id;
        }
        $ag = DB::table('reports')->select(DB::raw('sum(anggaran) as ag, status'))->where('status', 1)->where('activity_id', $ac)->groupBy('status')->value('ag');
        $jml = Activity::firstWhere('id', $ac);
        $total = ($ag / $jml->anggaran) * 100;
        return view('admin.grafik.cover', [
            'data' => Report::where('bulan', $request->bulan)->where('status', 1)->get(),
            'jml' => Report::where('bulan', $request->bulan)->where('status', 1)->count(),
            'pak' => Pak::firstWhere('id', $request->pak),
            'jb' => User::firstWhere('id', $request->user),
            'rfk' => $total
        ]);
    }

    public function getjadwal(Request $request)
    {
        $ac = Report::where('bulan', $request->bulan)->where('status', 1)->get();
        foreach ($ac as $i) {
            $ac = $i->activity_id;
        }
        $ag = DB::table('reports')->select(DB::raw('sum(anggaran) as ag, status'))->where('status', 1)->where('activity_id', $ac)->groupBy('status')->value('ag');
        $jml = Activity::firstWhere('id', $ac);
        $total = ($ag / $jml->anggaran) * 100;
        $sc =  Schedule::firstWhere('activity_id', $request->ac);
        return view('admin.grafik.jadwal', [
            'data' => Report::where('bulan', $request->bulan)->where('status', 1)->get(),
            'jml' => Report::where('bulan', $request->bulan)->where('status', 1)->count(),
            'target' => Target::where('schedule_id', $sc->id)->get(),
            'pak' => Pak::firstWhere('id', $request->pak),
            'jb' => User::firstWhere('id', $request->user),
            'rfk' => $total
        ]);
    }

    public function pengadaan()
    {
        return view('admin.grafik.pengadaan');
    }
    public function pengadaanData()
    {
        $data = Activity::select('id', 'user_id', 'pengadaan')->where('user_id', auth()->user()->id)->get()->groupBy('pengadaan');
        return response()->json(['data' => $data]);
    }
    public function sebaran()
    {
        return view('admin.grafik.sebaran');
    }
    public function laporan()
    {
        return view('admin.grafik.laporan');
    }
}
