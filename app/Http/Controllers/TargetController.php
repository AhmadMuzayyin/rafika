<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Report;
use App\Models\Target;
use App\Models\Schedule;
use App\Models\TKeuangan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TargetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        try {
            $cek = Target::firstWhere('schedule_id', $request->id);
            if ($cek == true) {
                $l = new Target();
                $target = $request->keuangan;
                $keuangan = $request->target;

                foreach($target as $k => $item){
                    $l->schedule_id = $request->id;
                    $l->bulan = $item[0];
                    $l->persentase = $item[1];
                    $l->progres = 100;
                }
                $l->save();
                
                $k = new TKeuangan();
                foreach($keuangan as $k => $item){
                    $k->schedule_id = $request->id;
                    $k->bulan = $item[0];
                    $k->anggaran = $item[1];
                    $k->progres = 100;
                }
                $k->save();
                return response()->json(['success', 'Data berhasil diupdate!']);
            }else{
                $target = Activity::firstWhere('id', $request->ac);
                $keuangan = $request->keuangan;

                $total = 0;
                foreach ($keuangan as $key => $item){
                    $total += $item[1];
                }

                // dd($total);
                foreach($keuangan as $k => $item){
                    $l = new Target();
                    $l->schedule_id = $request->id;
                    $l->bulan = $item[0];
                    $l->persentase = ($item[1] / $target->anggaran) * 100;
                    $l->progres = ($total / $target->anggaran) * 100;
                    $l->save();
                }
                
                foreach($keuangan as $k => $item){
                    $k = new TKeuangan();
                    $k->schedule_id = $request->id;
                    $k->bulan = $item[0];
                    $k->anggaran = $item[1];
                    $k->progres = ($total / $target->anggaran) * 100;
                    $k->save();
                }
                
                $pak = Activity::where('id', $request->ac)->get('pak_id');
                foreach($pak as $asu){
                    $idjancuk = $asu->pak_id;
                }
                foreach($keuangan as $item => $value){
                    $r = new Report();
                    $r->activity_id = $request->ac;
                    $r->user_id = Auth()->user()->id;
                    $r->pak_id = $idjancuk;
                    $r->bulan = $value[0];
                    $r->anggaran = $value[1];
                    $dana = Activity::firstWhere('id', $request->ac);
                    $r->dana = $dana->dana;
                    $r->kegiatan = $dana->nama;
                    $r->save();
                }
                $s = Schedule::find($request->id);
                if ($s->persentase == 60) {
                    $s->persentase = $s->persentase + 40;
                }else{
                    $s->persentase = 40;
                }
                $s->save();
                return response()->json(['success', 'Data berhasil disimpan!']);
            }
            return response()->json(['error', 'Data tidak dapat disimpan!']);

        } catch (\Throwable $th) {
            return response()->json(['tryError',$th->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Target  $target
     * @return \Illuminate\Http\Response
     */
    public function show(Target $target)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Target  $target
     * @return \Illuminate\Http\Response
     */
    public function edit(Target $target)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Target  $target
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Target $target)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Target  $target
     * @return \Illuminate\Http\Response
     */
    public function destroy(Target $target)
    {
        //
    }
}
