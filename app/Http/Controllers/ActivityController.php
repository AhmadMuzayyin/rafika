<?php

namespace App\Http\Controllers;

use App\Models\PPTK;
use App\Models\Report;
use App\Models\Target;
use App\Models\Volume;
use App\Models\Activity;
use App\Models\Location;
use App\Models\Schedule;
use App\Models\TKeuangan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth()->user()->isAdmin == true) {
           return view('admin.entry.kegiatan', [
            'page' => false,
            'data' => Activity::all(),
        ]); 
        }
        return view('admin.entry.kegiatan', [
            'page' => false,
            'data' => Activity::where('pak_id', session()->get('pak_id'))->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.entry.kegiatan',
        [
            'page' => 'create'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        try {
            $lp = json_encode($request->laporan);
            $a = new Activity;
            
            $a->pak_id = $request->pak_id;
            $a->user_id = Auth()->user()->id;
            $a->rek = $request->rek;
            $a->nama = $request->subkegiatan;
            $a->dana = $request->dana;
            $a->pengadaan = $request->pengadaan;
            $a->anggaran = $request->anggaran;
            $a->pelaksanaan = $request->pelaksanaan;
            $a->kegiatan = $request->kegiatan;
            $a->laporan = $lp;
            $a->program = $request->program;
            if ($request->anggaran >= 250000000) {
                $a->keterangan = "LELANG";
            }else{
                $a->keterangan = "NON LELANG";
            }
            $a->save();

            // dd($a->id);
            $s = new Schedule;
            $s->activity_id = $a->id;
            $s->persentase = 0;
            $s->save();

            $r = new Report();
            $r->activity_id = $a->id;
            $r->user_id = Auth()->user()->id;
            $r->pak_id = $request->pak_id;
            $r->dana = $request->dana;
            $r->save();
            
            return redirect()->route('activity.index')->with('success', 'Data berhasil ditambah!');
        //  return response()->json(['success', 'Data berhasil ditambah!']);

        } catch (\Throwable $th) {
            return response()->json(['tryError',$th->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        // dd($activity->id);
        return view('admin.entry.kegiatan', [
            'page' => "edit",
            'data' => Activity::firstWhere('id', $activity->id),
            'laporan' => Activity::where('id', $activity->id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $a = Activity::find($request->id);
            $a->rek = $request->rek;
            $a->nama = $request->subkegiatan;
            $a->anggaran = $request->anggaran;
            $a->pengadaan = $request->pengadaan;
            $a->pelaksanaan = $request->pelaksanaan;
            $a->dana = $request->dana;
            $a->kegiatan = $request->kegiatan;
            $a->save();

            return response()->json(['success', 'Data berhasil diupdate!']);
        } catch (\Throwable $th) {
            return response()->json(['tryError',$th->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        try {
            $sc = Schedule::firstWhere('activity_id', $activity->id);
            Activity::destroy($activity->id);
            DB::table('schedules')->where('activity_id', $activity->id)->delete();
            DB::table('locations')->where('schedule_id', $sc->id)->delete();
            DB::table('volumes')->where('schedule_id', $sc->id)->delete();
            DB::table('p_p_t_k_s')->where('schedule_id', $sc->id)->delete();
            DB::table('targets')->where('schedule_id', $sc->id)->delete();
            DB::table('t_keuangans')->where('schedule_id', $sc->id)->delete();
            DB::table('reports')->where('activity_id', $activity->id)->delete();
            return response()->json(['success', 'Data berhasil dihapus']);
        } catch (\Throwable $th) {
            return response()->json(['tryError',$th->getMessage()]);
        }
    }
}
