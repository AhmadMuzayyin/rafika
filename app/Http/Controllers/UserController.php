<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index', [
            'page' => 'index',
            'data' =>User::where('isAdmin', 0)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.index', [
            'page' => 'create'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        try {
            $user = new User;
            $fileName = time().'_'.$request->file('profil')->getClientOriginalName();
            $request->file('profil')->move(public_path('uploads'), $fileName);

            $user->kode_SKPD = $request->kode;
            $user->nama_SKPD = $request->skpd;
            $user->nama_operator = $request->namaoperator;
            $user->no_hp = $request->nooperator;
            $user->no_kantor = $request->nokantor;
            $user->alamat_kantor = $request->alamatkantor;
            $user->username = $request->username;
            $user->password = bcrypt($request->password);
            $user->nama_KPA = $request->kpa;
            $user->foto = $fileName;
            $user->level = $request->level;
            $user->save();

            return redirect()->route('user.index')->with('success', 'Data berhasil ditambah!');
        } catch (\Throwable $th) {
            return response()->json(['tryError',$th->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.user.index', [
            'page' => 'edit',
            'data' => User::where('id', $id)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());
        try {
            $user = User::find($request->id);
            $fileName = time().'_'.$request->file('profil')->getClientOriginalName();
            $request->file('profil')->move(public_path('uploads'), $fileName);

            $user->kode_SKPD = $request->kode;
            $user->nama_SKPD = $request->skpd;
            $user->nama_operator = $request->namaoperator;
            $user->no_hp = $request->nooperator;
            $user->no_kantor = $request->nokantor;
            $user->alamat_kantor = $request->alamatkantor;
            $user->username = $request->username;
            $user->password = bcrypt($request->password);
            $user->nama_KPA = $request->kpa;
            $user->foto = $fileName;
            $user->level = $request->level;
            $user->save();

            return redirect()->route('user.index')->with('success', 'Data berhasil ditambah!');
        } catch (\Throwable $th) {
            return response()->json(['tryError',$th->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            User::destroy($user->id);
            return response()->json(['success', 'Data berhasil dihapus']);
        } catch (\Throwable $th) {
            return response()->json(['tryError',$th->getMessage()]);
        }
    }
}
