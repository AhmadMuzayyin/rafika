<?php

namespace App\Http\Controllers;

use App\Models\LockPak;
use App\Models\Pak;
use Illuminate\Http\Request;

class PakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pak(){
        $sebelum = LockPak::where('sebelum', 1)->get();
        $sesudah = LockPak::where('sesudah', 1)->get();
        return view('admin.pak', [
            'sebelum' => $sebelum != null ? $sebelum : 0,
            'sesudah' => $sesudah != null ? $sesudah : 0
        ]);
    }
    public function index()
    {
        return view('admin.PAK.index',[
            'data' => LockPak::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        try {
            $pak = new Pak();
            $pak->nama = date('Y', strtotime($request->nama));
            $pak->save();

            $lk = new LockPak();
            $lk->pak_id = $pak->id;
            $lk->sebelum = 0;
            $lk->sesudah = 0;
            $lk->save();


            return redirect()->route('pak.index')->with('success', 'Data Berhasil ditambah!');
        } catch (\Throwable $th) {
            return response()->json(['tryErr', $th->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pak  $pak
     * @return \Illuminate\Http\Response
     */
    public function show(Pak $pak)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pak  $pak
     * @return \Illuminate\Http\Response
     */
    public function edit(Pak $pak)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pak  $pak
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pak $pak)
    {
        //
    }
    public function redirect(){
        // dd($pak);
        return view('admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pak  $pak
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pak $pak)
    {
        try {
            $p = Pak::all();
            if ($p->count() == 1) {
                return response()->json(['error', 'Data PAK tidak boleh kosong!']);
            }else{
                $pk = Pak::firstWhere('id', $pak->id);
                if ($pk->status == 1) {
                    return response()->json(['error', 'Data PAK sedang aktif']);
                }

                Pak::destroy($pak->id);
                return response()->json(['success', 'Data berhasil dihapus']);
            }
        } catch (\Throwable $th) {
            return response()->json(['tryErr', $th->getMessage()]);   
        }
    }

    public function kunci(Request $request){
        // dd($request->all());
        try {
            if ($request->sebelum != null) {
                $kunci = LockPak::firstWhere('pak_id', $request->sebelum);
                if ($kunci) {
                    if ($kunci->sebelum == 0) {
                        $s = 1;
                    }else{
                        $s = 0;
                    }
                }
                $kunci->sebelum = $s;
                $kunci->save();
            }else{
                $kunci = LockPak::firstWhere('pak_id', $request->sesudah);
                if ($kunci) {
                    if ($kunci->sesudah == 0) {
                            $s = 1;
                        }else{
                            $s = 0;
                        }
                }
                $kunci->sesudah = $s;
                $kunci->save();
            }

           return redirect()->route('pak.index')->with('success', 'PAK dibuka!');
        } catch (\Throwable $th) {
            return response()->json(['tryErr', $th->getMessage()]);
        }
    }
}
