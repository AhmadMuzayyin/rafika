<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\Target;
use App\Models\Activity;
use App\Models\Schedule;
use App\Models\TKeuangan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.report.index', [
            'data' => Report::all(),
            'activity' => Activity::all(),
            'selected' => 0
        ]);
    }

    public function get(Request $request){
        // dd($request->all());
        try {
            $ac = Report::where('dana', $request->dana)->get('id');
            $id = '';
            foreach ($ac as $id){
                $id = $id->id;
            }
            if ($ac == true) {
                return response()->json([
                    'success' => 'Data succuess',
                    'report' => Report::where('dana', $request->dana)->where('bulan', $request->bulan)->where('user_id', Auth()->user()->id)->get(),
                    'anggaran' => Activity::where('id', $id)->get()
                ]);
            }else{
                return response()->json(['error', 'Data tidak ada!']);
            }

        } catch (\Throwable $th) {
            return response()->json(['tryError',$th->getMessage()]);
        }
    }
    
    public function getSelected(Request $request){
        return view('admin.report.index', [
            'selected' => Report::where('bulan', $request->bulan)->where('dana', $request->dana)->get(),
            'data' => Schedule::all(),
        ]);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $report = array(Report::where('bulan', $request->bulan)->where('dana', $request->dana)->get());
            switch ($request->bulan) {
                case 'januari':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                case 'februari':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $kl = Report::where('bulan', "januari")->get('kegiatan_sekarang');
                                $kb = Report::where('bulan', "januari")->get('keuangan_sekarang');
                                foreach($kl as $kg){
                                    $report->kegiatan_lalu = $kg->kegiatan_sekarang;
                                }
                                foreach($kb as $ku){
                                    $report->keuangan_lalu = $ku->keuangan_sekarang;
                                }
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                case 'maret':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $kl = Report::where('bulan', "februari")->get('kegiatan_sekarang');
                                $kb = Report::where('bulan', "februari")->get('keuangan_sekarang');
                                foreach($kl as $kg){
                                    $report->kegiatan_lalu = $kg->kegiatan_sekarang;
                                }
                                foreach($kb as $ku){
                                    $report->keuangan_lalu = $ku->keuangan_sekarang;
                                }
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                case 'april':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $kl = Report::where('bulan', "maret")->get('kegiatan_sekarang');
                                $kb = Report::where('bulan', "maret")->get('keuangan_sekarang');
                                foreach($kl as $kg){
                                    $report->kegiatan_lalu = $kg->kegiatan_sekarang;
                                }
                                foreach($kb as $ku){
                                    $report->keuangan_lalu = $ku->keuangan_sekarang;
                                }
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                case 'mei':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $kl = Report::where('bulan', "april")->get('kegiatan_sekarang');
                                $kb = Report::where('bulan', "april")->get('keuangan_sekarang');
                                foreach($kl as $kg){
                                    $report->kegiatan_lalu = $kg->kegiatan_sekarang;
                                }
                                foreach($kb as $ku){
                                    $report->keuangan_lalu = $ku->keuangan_sekarang;
                                }
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                case 'juni':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $kl = Report::where('bulan', "mei")->get('kegiatan_sekarang');
                                $kb = Report::where('bulan', "mei")->get('keuangan_sekarang');
                                foreach($kl as $kg){
                                    $report->kegiatan_lalu = $kg->kegiatan_sekarang;
                                }
                                foreach($kb as $ku){
                                    $report->keuangan_lalu = $ku->keuangan_sekarang;
                                }
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                case 'juli':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $kl = Report::where('bulan', "juni")->get('kegiatan_sekarang');
                                $kb = Report::where('bulan', "juni")->get('keuangan_sekarang');
                                foreach($kl as $kg){
                                    $report->kegiatan_lalu = $kg->kegiatan_sekarang;
                                }
                                foreach($kb as $ku){
                                    $report->keuangan_lalu = $ku->keuangan_sekarang;
                                }
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                case 'agustus':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $kl = Report::where('bulan', "juli")->get('kegiatan_sekarang');
                                $kb = Report::where('bulan', "juli")->get('keuangan_sekarang');
                                foreach($kl as $kg){
                                    $report->kegiatan_lalu = $kg->kegiatan_sekarang;
                                }
                                foreach($kb as $ku){
                                    $report->keuangan_lalu = $ku->keuangan_sekarang;
                                }
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                case 'september':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $kl = Report::where('bulan', "agustus")->get('kegiatan_sekarang');
                                $kb = Report::where('bulan', "agustus")->get('keuangan_sekarang');
                                foreach($kl as $kg){
                                    $report->kegiatan_lalu = $kg->kegiatan_sekarang;
                                }
                                foreach($kb as $ku){
                                    $report->keuangan_lalu = $ku->keuangan_sekarang;
                                }
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                case 'oktober':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $kl = Report::where('bulan', "september")->get('kegiatan_sekarang');
                                $kb = Report::where('bulan', "september")->get('keuangan_sekarang');
                                foreach($kl as $kg){
                                    $report->kegiatan_lalu = $kg->kegiatan_sekarang;
                                }
                                foreach($kb as $ku){
                                    $report->keuangan_lalu = $ku->keuangan_sekarang;
                                }
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                case 'november':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $kl = Report::where('bulan', "oktober")->get('kegiatan_sekarang');
                                $kb = Report::where('bulan', "oktober")->get('keuangan_sekarang');
                                foreach($kl as $kg){
                                    $report->kegiatan_lalu = $kg->kegiatan_sekarang;
                                }
                                foreach($kb as $ku){
                                    $report->keuangan_lalu = $ku->keuangan_sekarang;
                                }
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                case 'desember':
                        foreach($report as $k => $v){
                            foreach($v as $key => $value){
                                // dd($value);
                                $data = $request->all();
                                $id = 'id-'.$value->id;
                                $kegiatan = 'kegiatan-'.$value->id;
                                $keuangan = 'keuangan-'.$value->id;
                                $kendala = 'kendala-'.$value->id;
                                // dd($id);
                                $report = Report::find($data[$id]);
                                $kl = Report::where('bulan', "november")->get('kegiatan_sekarang');
                                $kb = Report::where('bulan', "november")->get('keuangan_sekarang');
                                foreach($kl as $kg){
                                    $report->kegiatan_lalu = $kg->kegiatan_sekarang;
                                }
                                foreach($kb as $ku){
                                    $report->keuangan_lalu = $ku->keuangan_sekarang;
                                }
                                $report->kegiatan_sekarang = ($data[$keuangan] / $report->anggaran) * 100;
                                $report->keuangan_sekarang = $data[$keuangan];
                                $report->kendala = $data[$kendala];
                                $report->status = 1;
                                $report->save();
                            }
                        }
                    break;
                default:
                    return redirect()->route('report.index');
                    break;
            }

        return redirect()->route('report.index');
        } catch (\Throwable $th) {
            return response()->json(['tryErr', $th->getMessage()]);
        }
    }
}
