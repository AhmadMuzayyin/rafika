<?php

namespace App\Http\Controllers;

use App\Models\InputActivation;
use Illuminate\Http\Request;

class InputActivationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InputActivation  $inputActivation
     * @return \Illuminate\Http\Response
     */
    public function show(InputActivation $inputActivation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InputActivation  $inputActivation
     * @return \Illuminate\Http\Response
     */
    public function edit(InputActivation $inputActivation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InputActivation  $inputActivation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InputActivation $inputActivation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InputActivation  $inputActivation
     * @return \Illuminate\Http\Response
     */
    public function destroy(InputActivation $inputActivation)
    {
        //
    }
}
