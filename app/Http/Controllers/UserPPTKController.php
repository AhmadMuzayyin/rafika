<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use App\Models\UserPPTK;
use Illuminate\Http\Request;

class UserPPTKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    try {
        $up = new UserPPTK;
        $up->nama = $request->nama;
        $up->nip = $request->nip;
        $up->save();

        return redirect('/schedule/'. $request->id .'/edit/pptk')->with('success', 'Data berhasil ditambah');
    } catch (\Throwable $th) {
        return response()->json(['tryError',$th->getMessage()]);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserPPTK  $userPPTK
     * @return \Illuminate\Http\Response
     */
    public function show(UserPPTK $userPPTK)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserPPTK  $userPPTK
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPPTK $userPPTK)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserPPTK  $userPPTK
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserPPTK $userPPTK)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserPPTK  $userPPTK
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPPTK $userPPTK)
    {
        //
    }
}
