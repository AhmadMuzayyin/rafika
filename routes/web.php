<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GrafikController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\Login;
use App\Http\Controllers\PakController;
use App\Http\Controllers\PenggunaAnggaranController;
use App\Http\Controllers\PPTKController;
use App\Http\Controllers\PrintController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SchuduleController;
use App\Http\Controllers\TargetController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserPPTKController;
use App\Http\Controllers\VolumeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Login::class, 'index'])->name('login');
Route::post('/auth', [Login::class, 'auth'])->name('auth');

Route::middleware(['auth'])->group(function () {
    Route::post('/logout', [Login::class, 'logout'])->name('logout');
    Route::get('/pak/anggaran', [PakController::class, 'pak'])->name('PAK');
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/dashboard-data', [DashboardController::class, 'indexData'])->name('dashboard-data');
    Route::resources([
        '/activity' => ActivityController::class,
        '/schedule' => SchuduleController::class,
        '/volume' => VolumeController::class,
        '/UserPPTK' => UserPPTKController::class,
        '/pptk' => PPTKController::class,
        '/location' => LocationController::class,
        '/target' => TargetController::class,
        '/report' => ReportController::class,
        '/user' => UserController::class,
        '/pak' => PakController::class,
        '/pa' => PenggunaAnggaranController::class,
    ]);

    Route::post('/activity/update', [ActivityController::class, 'update'])->name('kegiatan.up');
    Route::post('/user/update', [UserController::class, 'update'])->name('user.up');
    Route::get('/schedule/{schedule}/edit/volume', [SchuduleController::class, 'volume']);
    Route::get('/schedule/{schedule}/edit/pptk', [SchuduleController::class, 'pptk']);
    Route::get('/schedule/{schedule}/edit/target', [SchuduleController::class, 'target']);

    Route::post('/getreport', [ReportController::class, 'get']);
    Route::post('/report/update', [ReportController::class, 'update'])->name('report.up');
    Route::post('/pak/kunci', [PakController::class, 'kunci'])->name('pak.unlock');
    Route::get('/dashbord', [PakController::class, 'redirect'])->name('p-to-d');

    // Grafik
    Route::get('/rekapitulasi', [GrafikController::class, 'rekap'])->name('rekap');
    Route::post('/arsip', [GrafikController::class, 'getArsip'])->name('get.arsip');
    Route::get('/arsip', [GrafikController::class, 'arsip'])->name('arsip');
    Route::post('/arsip/cover', [GrafikController::class, 'getCover'])->name('get.Cover');
    Route::post('/arsip/jadwal', [GrafikController::class, 'getJadwal'])->name('get.Jadwal');

    Route::get('/pengadaan', [GrafikController::class, 'pengadaan'])->name('pengadaan');
    Route::get('/pengadaan-data', [GrafikController::class, 'pengadaanData'])->name('pengadaan-data');

    Route::get('/sebaran', [GrafikController::class, 'sebaran'])->name('sebaran');
    Route::get('/laporan', [GrafikController::class, 'laporan'])->name('laporan');
    Route::get('/program', [GrafikController::class, 'program'])->name('program');
    Route::post('/get-selected', [ReportController::class, 'getSelected']);

    // Print
    Route::get('/print/dak', [PrintController::class, 'PrintDAK'])->name('print.DAK');
});
